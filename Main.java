public class Main {
    public static void main(String[] args) {
        
        // instantiate object for cat and dog
        Crow Cr = new Crow();
        Cat c = new Cat();

        System.out.println("****\tCrow Profile");
        Cr.name = "blackboy";
        System.out.println("Crow name : "+Cr.name);

        // inherited property
        Cr.eat();
        Cr.sleep();
        // method overriding
        // use of SUPER keyword
        Cr.DisplayWhoAmI();;
        // method overloading
        c.makeSound("ka--ka",3);
        


        System.out.println("****\tCat Profile");
        c.name = "mauu";
        System.out.println("cat name : "+c.name);
       
        // inherited property
        c.eat();
        c.sleep();
        // method overriding
        c.DisplayWhoAmI();
        // method overloading
        c.makeSound("mew",2);
        
       
       
    }
}
