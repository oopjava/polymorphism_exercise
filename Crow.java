public class Crow extends Animal {
    public void canFly() {
        System.out.println("I can Fly!");
    }

    // method override
    public void DisplayWhoAmI()
    {
        System.out.println("I am CROW");
    }
    // method overloading
    public void makeSound(String call, int times) {
        System.out.println("Hi I can make "+call+" for "+times+" seconds!");
    } 
}
