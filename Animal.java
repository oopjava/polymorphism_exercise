public class Animal {
    // name of animal
    String name;

    public void eat() {
        System.out.println("I can eat");
    }
    public void sleep() {
        System.out.println("I can sleep");
    }

    // example for method overloading
    public void makeSound(String call) {
        System.out.println("Hi "+call);
    }

    // example for method overriding
    public void DisplayWhoAmI() {
        System.out.println("I am Animal");
    }
    

}
