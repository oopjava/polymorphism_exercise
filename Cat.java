public class Cat extends Animal {
    public void canBite() {
        System.out.println("Hi! I can bite");
    }

    // method override
    public void DisplayWhoAmI()
    {
        // super keyword for displaying super info
        super.DisplayWhoAmI();
        System.out.println("I am CROW");
    }

    // method overloading
    public void makeSound(String call, int times) {
        System.out.println("Hi I can make "+call+" for "+times+" seconds!");
    }
}
